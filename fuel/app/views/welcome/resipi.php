<!DOCTYPE html>
<html>
    <head>
        <title>レシピのアプリ</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            li{
                list-style:none;
                margin-right:24px;
            }
            
            a{
                text-decoration:none;
                color:#333333;
            }
            
            a:hover{
                text-decoration:underline;
            }
            p{
                color:#333333;
                font-size:0.75em;
            }
            
            img{
                width:100%;
                height:auto;
            }
            
            nav> ul{
                display:flex;
                justify-content:space-between;
                margin:0 32% 0 32%;
            }
            
            .box ul{
                display:flex;
                justify-content:space-between;
            }
            
            .box li{
                width:32%;
                height:auto;
            }
            .box p{
                text-align:center;
            }
        </style>
    </head>
    <body>
        <header>
            <h1>レシピのアプリ</h1>
        </header>
        <div id="category">
            <nav>
                <ul>
                    <li><a class="all" data-filter="all" href="#">すべて</a></li>
                    <li><a class="rice" data-filter="rice" href="#">主食</a></li>
                    <li><a class="meat" data-filter="meat" href="#">肉料理</a></li>
                    <li><a class="vegetable" data-filter="vegetable" href="#">野菜料理</a></li>
                    <li><a class="fish" data-filter="fish" href="#">魚料理</a></li>
                </ul>
            </nav>
        </div>
        <div id="main">
            <div class="box">
                <ul>
                    <?php foreach($resipi as $re):?>
                    <li>
                        <a href="#">
                            <img src="<?php echo Uri::base().$re['image']; ?>">
                            <p><?php echo $re['name']; ?></p>
                        </a>
                        <form action="<?php echo Uri::create('welcome/favorite'); ?>" method="post">
                            <input type="hidden" name="resipi_id" value="<?= $re['id'] ?>">
                            <?php if(isset($favorite[$re['id']])): ?>
                            ♥
                            <?php else: ?>
                            <input type="submit" name="favorite" value="お気に入り">
                            <?php endif; ?>
                        </form> 
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </body>
</html>
