<style>
    td img{
        width:300px;
        height:auto;
    }
</style>
一覧表示

<table>
    <tr>
        <th>ID</th>
        <th>レシピ名</th>
        <th>概要</th>
        <th>材料</th>
        <th>画像</th>
        <th>手順</th>
    </tr>
    <?php foreach($resipi as $re):?>
    <tr>
        <td><?php echo $re['id']; ?></td>
        <td><?php echo $re['name']; ?></td>
        <td><?php echo $re['outline']; ?></td>
        <td><?php echo $re['materials']; ?></td>
        <td><img src="<?php echo Uri::base().$re['image']; ?>"></td>
         <td><?php echo $re['process']; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php

var_dump($resipi);