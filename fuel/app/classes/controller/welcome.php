<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.8
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2016 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Welcome extends Controller
{
    public function action_list()
    {
        $data = array();
        $data['resipi'] = Model_Resipi::find('all');
        $favorite = Model_Favorite::find('all', array('where' => array( array('user_id', 1) )));
        $data['favorite'] = Arr::assoc_to_keyval($favorite, 'resipi_id', 'resipi_id');
        return Response::forge(View::forge('welcome/resipi',$data));
    }
    
    public function get_upload()
    {
        $data = array();
        $data['name'] = '';
        $data['outline'] = '';
        $data['materials'] = '';
        $data['process'] = '';

        return Response::forge(View::forge('welcome/upload',$data)); 
    }
    
    public function post_upload()
    {
        $image = Input::file('image');
        if($image['name']!=""){
            //var_dump($image);
            move_uploaded_file($image['tmp_name'],'assets/img/'.$image['name']);
            
            $model = Model_Resipi::forge();
            $model->image = 'assets/img/'.$image['name'];
            $model->upload_datetime = date('Y-m-d H:i:s');
            
            $model->name = Input::post('name','');
            $model->outline = Input::post('outline','');
            $model->materials = Input::post('materials','');
            $model->process = Input::post('process','');
            
            $model->save();
            
            Response::redirect('welcome/list');
        }else{
            Response::redirect('welcome/upload');
        }
        
    }
    public function post_favorite(){
        $model = Model_Favorite::forge();
        $model->user_id =(1);
        $model->resipi_id = Input::post('resipi_id','');
        $model->save();
        Response::redirect('welcome/list');
    }
    /**
	 * The basic welcome message
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		return Response::forge(View::forge('welcome/index'));
	}

	/**
	 * A typical "Hello, Bob!" type example.  This uses a Presenter to
	 * show how to use them.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_hello()
	{
		return Response::forge(Presenter::forge('welcome/hello'));
	}

	/**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(Presenter::forge('welcome/404'), 404);
	}
}
