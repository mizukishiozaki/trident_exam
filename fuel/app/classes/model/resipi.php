<?php

class Model_Resipi extends \Orm\Model
{
	protected static $_properties = array(
		
		'id',
		'name',
		'outline',
		'materials',
		'image',
                'process',
	);


	protected static $_table_name = 'resipi';

}
